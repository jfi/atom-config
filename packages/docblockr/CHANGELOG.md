# 0.5.1 (28 May 2014)

* Scope selector bug fix

# 0.5.0 (26 May 2014)

* Named regex added to fix parser errors
* Multiline comment decorate feature added

# 0.4.2 (9 May 2014)

* Scope based checks added

# 0.4.1 (1 May 2014)

* Readme fix
* Changelog Update

# 0.4.0 (1 May 2014)

* Language support ported for all languages
* Moved all files to javascript
* Support for all parent package functionalities
* Macros remain unsupported

# 0.3.0 (27 Apr 2014)

* Make parser library editor independent

# 0.1.0 - 0.2.1 (26 Apr 2014)

* Initial release
